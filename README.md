# EzAvcLan Arduino library
## Arduino library for [AVC-LAN](https://elinux.org/AVC-LAN) protocol used in (usually) Japanise cars.
---
Prepared for:
* Arduino UNO board (atmega 328p and probably similar)
* ESP8266 (not tested yet)
* ESP32 (can cause random errors in transmision, probably due to unhandled internal ESP interrupts)
---
Usage as in `ez_avc_lan.ino`


## Libraries used to create EzAvcLan

Based mostly on avc lan theory of Louis Frigon "AVC Lan driver for Toyota devices"
>  Description  :  AVC Lan driver for Toyota devices.  
  Author       :  Louis Frigon  
  Copyright    :  (c) 2007 SigmaObjects



Created based on work metioned above by [Szymon Ruta](https://gitlab.com/Shymon)
