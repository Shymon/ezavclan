#include "src/ez_avc_lan.h"


#ifdef ARDUINO_AVR_UNO
  EzAvcLan AvcLan = EzAvcLan(5, 4, 5, 4, &PORTD, &PIND); 
#endif
#if defined(ARDUINO_ESP8266_NODEMCU)
  EzAvcLan AvcLan = EzAvcLan(5, 4); 
#endif
#if defined(ARDUINO_ESP32_DEV)
  EzAvcLan AvcLan = EzAvcLan(2, 14); 
#endif

void setup() {
  Serial.begin(115200);

  pinMode(13, OUTPUT);
  AvcLan.start();

  Serial.println("Hello, this is simple avc lan simulator");
  Serial.println("By Szymon Ruta <gitlab.com/Shymon>");
}


AvcLanFrame frame {
  0x164,
  0xFFE,
  0xD,
  0x2,
  { 0x2, 0xF0 }
};


void prettyPrint(const String &s, const uint32_t &n) {
  Serial.print(s);
  Serial.print(n, HEX);
  Serial.print(", BIN: ");
  Serial.println(n, BIN);
}


void loop() {
  bool read = true;

  if (read) { // read
    auto error = AvcLan.readFrame(&frame);
    if (error != EzAvcLan::NoError) {
      Serial.println("Error: " + String(error) + "."); 
    }
    else {
      prettyPrint("Master Address: ", frame.master_address);
      prettyPrint("Slave Address:  ", frame.slave_address);
      prettyPrint("Control:        ", frame.control);
      prettyPrint("Data length:    ", frame.data_length);
      for (uint8_t i = 0; i < frame.data_length; i++) {
        prettyPrint("Data " + String(i) + ": ", frame.data[i] );
      }
    }
    delay(1);
  } else {    // send
    delay(500);
    Serial.println("Sending frame...");
    delay(300);

    AvcLan.sendFrame(frame);

    delay(200);
    Serial.println("Frame sent! Awaiting till next frame...");
    delay(1000);
  }
}
