// Ez Avc Lan 
// By Szymon Ruta <gitlab.com/Shymon>

// AVC LAN THEORY BELOW BY Louis Frigon
// Description  :  AVC Lan driver for Toyota devices.
// Author       :  Louis Frigon
// Copyright    :  (c) 2007 SigmaObjects

/*--------------------------------------------------------------------------------------------------

 //                      |<---- Bit '0' ---->|<---- Bit '1' ---->|
     Physical '1'      ,---------------,   ,---------,         ,---------
                       ^               |   ^         |         ^
     Physical '0' -----'               '---'         '---------'--------- Idle low
                       |---- 33 us ----| 7 |- 20 us -|- 20 us -|

     A bit '0' is typically 33 us high followed by 7 us low.
     A bit '1' is typically 20 us high followed by 20 us low.
     A start bit is typically 165 us high followed by 30 us low.

--------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------
    Frame

        1      1        12        1       12        1   1     4     1   1      8     1   1     8      1   1           8      1   1                                
    |------|------|-------------|---|-------------|---|---|-------|---|---|--------|---|---|--------|---|---| ... |--------|---|---|                                                                                        
     Header   BCB   Master addr.  P   Slave Addr.   P   A  Control  P   A    Data    P   A    DATA_1  P   A         DATA_N   P   A                                                
                                                             Bits           length
    BCB - Broadcast bit

--------------------------------------------------------------------------------------------------------------------*/

// Todos:
// TODO: NOT CHECKING IF ACKNOWLEDGED

#include "ez_avc_lan.h"

#if defined(ARDUINO_ESP32_DEV)
  #include <Esp.h>
  auto esp_32_instance = EspClass();
#endif

constexpr uint16_t EzAvcLan::_3us_timer_value;          
constexpr uint16_t EzAvcLan::_5us_timer_value;          
constexpr uint16_t EzAvcLan::_7us_timer_value;          
constexpr uint16_t EzAvcLan::_15us_timer_value;
constexpr uint16_t EzAvcLan::_17us_timer_value;
constexpr uint16_t EzAvcLan::_20us_timer_value;
constexpr uint16_t EzAvcLan::_almost20us_timer_value;
constexpr uint16_t EzAvcLan::_33us_timer_value;
constexpr uint16_t EzAvcLan::_200us_timer_value;

constexpr uint8_t EzAvcLan::master_address_length;
constexpr uint8_t EzAvcLan::slave_address_length;
constexpr uint8_t EzAvcLan::control_length;
constexpr uint8_t EzAvcLan::data_length_length;
constexpr uint8_t EzAvcLan::data_length;

void EzAvcLan::sendFrame(const AvcLanFrame &frame) {

  uint8_t master_parity = calcParity(frame.master_address),
          slave_parity = calcParity(frame.slave_address),
          control_parity = calcParity(frame.control),
          data_length_parity = calcParity(frame.data_length);

  uint8_t data_parities[frame.data_length];
  for(uint8_t i = 0; i < frame.data_length; i++) {
    data_parities[i] = calcParity(frame.data[i]);
  }

  noInterrupts();
  // Header 
  sendHeaderBit(1);
  // Broadcast bit
  sendBit(0);
  // Master addr
  sendBits(frame.master_address, master_address_length);
  sendBit(master_parity);
  // Slave addr
  sendBits(frame.slave_address, slave_address_length);
  sendBit(slave_parity);
  sendAck();
  // Control
  sendBits(frame.control, control_length);
  sendBit(control_parity);
  sendAck();
  // Data length
  sendBits(frame.data_length, data_length_length);
  sendBit(data_length_parity);
  sendAck();
  // Data
  for(uint8_t i = 0; i < frame.data_length; i++) {
    sendBits(frame.data[i], data_length);
    sendBit(data_parities[i]);
    sendAck();
  }
  finish();
  interrupts();
}

void EzAvcLan::start() {
  preparePins();
  prepareTimer();
}

inline void EzAvcLan::set_high() {
  #ifdef ARDUINO_AVR_UNO
    *ports_tx |= port_txb;
  #endif
  #if defined(ARDUINO_ESP8266_NODEMCU) || defined(ARDUINO_ESP32_DEV)
    digitalWrite(port_tx, 1);
  #endif
}

inline void EzAvcLan::set_low() {
  #ifdef ARDUINO_AVR_UNO
    *ports_tx &= !port_txb;
  #endif
  #if defined(ARDUINO_ESP8266_NODEMCU) || defined(ARDUINO_ESP32_DEV)
    digitalWrite(port_tx, 0);
  #endif

}

void EzAvcLan::prepareTimer() {
  #ifdef ARDUINO_AVR_UNO
    noInterrupts();
    
    TCCR1A = 0;
    TCCR1B = 0;
    TCNT1 = 0; // Initial value

    TCCR1B |= (1 << CS10);

    interrupts();
  #endif
}

void EzAvcLan::preparePins() {
  #ifdef ARDUINO_AVR_UNO
    pinMode(port_tx_arduino, OUTPUT);
    pinMode(port_rx_arduino, INPUT_PULLUP);
  #endif
  #if defined(ARDUINO_ESP8266_NODEMCU) || defined(ARDUINO_ESP32_DEV)
    pinMode(port_tx, OUTPUT);
    pinMode(port_rx, INPUT_PULLUP);
  #endif
}

inline void EzAvcLan::reset_timer() {
  #ifdef ARDUINO_AVR_UNO
    TCNT1 = 0;
  #endif
  #if defined(ARDUINO_ESP8266_NODEMCU)
    lastTimerValue = esp_get_cycle_count();
  #endif
  #if defined(ARDUINO_ESP32_DEV)
    lastTimerValue = esp_32_instance.getCycleCount();
  #endif
}

inline volatile uint16_t EzAvcLan::read_timer() {
  #ifdef ARDUINO_AVR_UNO
    return TCNT1;
  #endif
  #if defined(ARDUINO_ESP8266_NODEMCU) || defined(ARDUINO_ESP32_DEV)
      #if defined(ARDUINO_ESP8266_NODEMCU)
        uint32_t currVal = esp_get_cycle_count();
      #endif
      #if defined(ARDUINO_ESP32_DEV)
        uint32_t currVal = esp_32_instance.getCycleCount();
      #endif
      if (currVal < lastTimerValue) {
        return UINT32_MAX - lastTimerValue + currVal;
      } else {
        return currVal - lastTimerValue;
      }

  #endif
}

inline void EzAvcLan::ready_to_send_next_bit() {
  if (lastSendBit) {
    while(read_timer() < _20us_timer_value);
    return;
  }

  while(read_timer() < _7us_timer_value);
}

inline void EzAvcLan::last_bit_ended() {
  if (lastSendBit) {
    while(read_timer() < _20us_timer_value);
    return;
  }

  while(read_timer() < _33us_timer_value);
}

void EzAvcLan::finish() {
  last_bit_ended();
  set_low();
}

void EzAvcLan::sendBit(const uint8_t &bit) {
  last_bit_ended();
  set_low();
  reset_timer();

  ready_to_send_next_bit();
  set_high();

  reset_timer();
  lastSendBit = bit;
}

void EzAvcLan::sendHeaderBit(const uint8_t &bit) {
  set_high();
  reset_timer();
  while(read_timer() < 8 * _20us_timer_value + 3 * _5us_timer_value); // 165 us
  // Next function takes care of finishing

  lastSendBit = bit;
}

void EzAvcLan::sendBits(const uint32_t &bits, const uint8_t &length) {
  for(Gi = 0; Gi < length; Gi++) {
    sendBit(bits >> length - 1 - Gi & 0x1); // Read from MSB
  }
}

// Returns 1 if partity, 0 if not parity
uint8_t EzAvcLan::calcParity(const uint32_t &n) {
  int y;
  y= n ^ (n >> 1);
  y = y ^ (y >> 2);
  y = y ^ (y >> 4);
  y = y ^ (y >> 8);
  y = y ^ (y >> 16);
  //checking the rightmost bit
  if (y & 1)
      return 0;
  return 1;
}

void EzAvcLan::sendAck() {
  // TODO: NOT CHECKING IF ACKNOWLEDGED
  sendBit(0);
}

EzAvcLan::ReadError EzAvcLan::readFrame(AvcLanFrame *frame) {
  FullAvcLanFrame fullAvcLanFrame; 
  ReadError error;
  uint8_t bit;

  // Header
  error = readBitLongWait(&fullAvcLanFrame.header);
  if (error != NoError) return error;

  // Broadcast
  error = readBit(&fullAvcLanFrame.broadcast);
  if (error != NoError) return error;
  
  // MasterAddress
  error = readBits(&fullAvcLanFrame.master_address, master_address_length);
  if (error != NoError) return error;
  // Parity
  error = readBit(&bit);
  if (error != NoError) return error;
  if (calcParity(fullAvcLanFrame.master_address) != bit) {
    Serial.println("M parity: " + String(calcParity(fullAvcLanFrame.master_address)) + "  vs  " + String(bit)); 
    return InvalidMasterParity; 
  }
  frame->master_address   = fullAvcLanFrame.master_address;

  while(is_low()); // Wait for high
  // SlaveAddress
  error = readBits(&fullAvcLanFrame.slave_address, slave_address_length);
  if (error != NoError) return error;
  // Parity
  error = readBit(&bit);
  if (error != NoError) return error;
  if (calcParity(fullAvcLanFrame.slave_address) != bit) {
    Serial.println("S parity: " + String(calcParity(fullAvcLanFrame.slave_address)) + "  vs  " + String(bit)); 
    return InvalidSlaveParity; 
  }

  error = acknowledge();
  if (error != NoError) return error;
  frame->slave_address    = fullAvcLanFrame.slave_address;

  while(is_low()); // Wait for high
  // Control
  error = readBits(&fullAvcLanFrame.control, control_length);
  if (error != NoError) return error;
  // Parity
  error = readBit(&bit);
  if (error != NoError) return error;
  if (calcParity(fullAvcLanFrame.control) != bit){
    Serial.println("C parity: " + String(calcParity(fullAvcLanFrame.control)) + "  vs  " + String(bit)); 
    return InvalidControlParity; 
  }
  error = acknowledge();
  if (error != NoError) return error;
  frame->control          = fullAvcLanFrame.control;

  while(is_low()); // Wait for high
  // Data length
  error = readBits(&fullAvcLanFrame.data_length, data_length_length);
  if (error != NoError) return error;
  // Mock reading parity
  error = readBit(&bit);
  if (error != NoError) return error;
  if (calcParity(fullAvcLanFrame.data_length) != bit){
    Serial.println("DL parity: " + String(calcParity(fullAvcLanFrame.data_length)) + "  vs  " + String(bit)); 
    return InvalidDataLengthParity; 
  }
  error = acknowledge();
  if (error != NoError) return error;
  frame->data_length      = fullAvcLanFrame.data_length;

  while(is_low()); // Wait for high
  if (fullAvcLanFrame.data_length > EZ_AVC_LAN_MAX_DATA_LENGTH) return OOM;
  uint8_t data_frame;
  for(uint8_t i = 0; i < fullAvcLanFrame.data_length; i++) {
    while(is_low()); // Wait for high
    // Data
    error = readBits(&fullAvcLanFrame.data[i], data_length);
    if (error != NoError) return error;
    // Mock reading parity
    error = readBit(&bit);
    if (calcParity(fullAvcLanFrame.data[i]) != bit){
      Serial.println("D parity: " + String(calcParity(fullAvcLanFrame.data[i])) + "  vs  " + String(bit)); 
      return InvalidDataParity; 
    }
    if (error != NoError) return error;
    error = acknowledge();
    if (error != NoError) return error;
  }
  memcpy(frame->data, fullAvcLanFrame.data, fullAvcLanFrame.data_length);

  return NoError;
}

inline volatile uint8_t EzAvcLan::is_high() { 
  #ifdef ARDUINO_AVR_UNO
    return *ports_rx & port_rxb; 
  #endif
  #if defined(ARDUINO_ESP8266_NODEMCU) || defined(ARDUINO_ESP32_DEV)
    return digitalRead(port_rx);
  #endif
};
inline volatile uint8_t EzAvcLan::is_low() { return !is_high(); };

// We assume that line is 1 already
EzAvcLan::ReadError EzAvcLan::readBit(uint8_t *bit) {
  uint16_t timeout = 2 * _20us_timer_value;
  ReadError error;
  
  // 1
  reset_timer();
  while(read_timer() < timeout && is_high()); // Wait for 0 or timeout
  if (is_high()) return Timeout;
  reset_timer();
  
  error = readBitValue(bit);
  if (error != NoError) return error;

  return NoError;
}

template<class T> EzAvcLan::ReadError EzAvcLan::readBits(T *bits, const uint8_t &length) {
  ReadError error;
  uint8_t bit;
  *bits = 0x00;

  for (Gi = 0; Gi < length; Gi ++) {
    *bits <<= 1; // Insert new bit
    error = readBit(&bit);
    if (error != NoError) return error;

    *bits |= bit;
  } 

  return NoError;
}

EzAvcLan::ReadError EzAvcLan::acknowledge() {
  // Acknowledge works in a way that sender holds 1 for 20us
  // and receiver holds for additional 12, up to 33 us
  
  // Hold 1 until 33us
  set_high();
  while(read_timer() < _33us_timer_value - 47); // UNO ACKNOWLEDGED HOLD VALUE
  set_low();
}

EzAvcLan::ReadError EzAvcLan::readBitLongWait(uint8_t *bit) {
  uint16_t long_timeout = _200us_timer_value;
  ReadError error;
  
  // 0
  while(is_low()); // Wait for 1
  // 1
  reset_timer();
  while(read_timer() < long_timeout && is_high()); // Wait for 0 or timeout
  if (is_high()) return HeaderTimeout;
  reset_timer();
  
  error = readBitValue(bit);
  if (error != NoError) return error;

  return NoError;  
}

EzAvcLan::ReadError EzAvcLan::readBitValue(uint8_t *bit) {
  while(read_timer() < _33us_timer_value && is_low()); // Wait for 1, or timeout zero value
  if (is_low()) return OneTooLong;

  uint16_t zeroTime = read_timer();
  reset_timer();
  if (zeroTime < _3us_timer_value) return ZeroTooShort;
  if (zeroTime < _17us_timer_value) {
    *bit = 0;
  } else {
    *bit = 1;
  }

  return NoError;
}
