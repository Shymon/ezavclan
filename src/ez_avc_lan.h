// Ez Avc Lan
// By Szymon Ruta <gitlab.com/Shymon>

// AVC LAN THEORY BELOW BY Louis Frigon
// Description  :  AVC Lan driver for Toyota devices.
// Author       :  Louis Frigon
// Copyright    :  (c) 2007 SigmaObjects

/*--------------------------------------------------------------------------------------------------

 //                      |<---- Bit '0' ---->|<---- Bit '1' ---->|
     Physical '1'      ,---------------,   ,---------,         ,---------
                       ^               |   ^         |         ^
     Physical '0' -----'               '---'         '---------'--------- Idle low
                       |---- 33 us ----| 7 |- 20 us -|- 20 us -|

     A bit '0' is typically 33 us high followed by 7 us low.
     A bit '1' is typically 20 us high followed by 20 us low.
     A start bit is typically 165 us high followed by 30 us low.

--------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------
    Frame

        1      1        12        1       12        1   1     4     1   1      8     1   1     8      1   1           8      1   1                                
    |------|------|-------------|---|-------------|---|---|-------|---|---|--------|---|---|--------|---|---| ... |--------|---|---|                                                                                        
     Header   BCB   Master addr.  P   Slave Addr.   P   A  Control  P   A    Data    P   A    DATA_1  P   A         DATA_N   P   A                                                
                                                             Bits           length
    BCB - Broadcast bit

--------------------------------------------------------------------------------------------------------------------*/

// Todos:
// TODO: NOT CHECKING IF ACKNOWLEDGED
#pragma once

#if defined(ARDUINO_AVR_UNO) || defined(ARDUINO_ESP8266_NODEMCU) || defined(ARDUINO_ESP32_DEV)
#else
  #error NO INTENDED TO WORK WITH OTHER BOARDS
#endif

#include <Arduino.h>

#define EZ_AVC_LAN_MAX_DATA_LENGTH 16

struct AvcLanFrame {
  uint16_t master_address; // 12 Bits
  uint16_t slave_address;  // 12 Bits
  uint8_t  control;        //  4 Bits
  uint8_t  data_length;    //  8 Bits
  // Max data length is EZ_AVC_LAN_MAX_DATA_LENGTH, don't want to bother with memory right now
  char  data[EZ_AVC_LAN_MAX_DATA_LENGTH];                // data_length Bytes (8 * data_length Bits) 
};

struct FullAvcLanFrame {
  uint8_t  header;         //  1 Bit 
  uint8_t  broadcast;      //  1 Bit 
  uint16_t master_address; // 12 Bits
  uint16_t slave_address;  // 12 Bits
  uint8_t  control;        //  4 Bits
  uint8_t  data_length;    //  8 Bits
  // Max data length is EZ_AVC_LAN_MAX_DATA_LENGTH, don't want to bother with memory right now
  char     data[EZ_AVC_LAN_MAX_DATA_LENGTH];  // data_length Bytes (8 * data_length Bits)
};

class EzAvcLan {
  public:
    enum ReadError {
      NoError                 =  0,
      HeaderTimeout           =  1,
      Timeout                 =  2,
      ZeroTooShort            =  3,
      OneTooLong              =  4,
      OOM                     =  5,
      InvalidMasterParity     = 20,
      InvalidSlaveParity      = 21,
      InvalidControlParity    = 22,
      InvalidDataLengthParity = 23,
      InvalidDataParity       = 24,
      Test                    = 99
    };

    #ifdef ARDUINO_AVR_UNO
      EzAvcLan(
        const uint8_t &_port_tx, // Port number in port registry (0 - 7)
        const uint8_t &_port_rx, // Port number in port registry (0 - 7)
        const uint8_t &_port_tx_arduino, // Port number in arduino registry (0 - 13 or smth)
        const uint8_t &_port_rx_arduino, // Port number in arduino registry (0 - 13 or smth)
        volatile uint8_t* _ports_tx, // Port registry for tx e.g. PORTD
        volatile uint8_t* _ports_rx  // Port registry for rx e.g. PORTD
      ) :
      port_rx(_port_rx),
      port_tx(_port_tx),
      port_rx_arduino(_port_rx),
      port_tx_arduino(_port_tx),
      ports_tx(_ports_tx),
      ports_rx(_ports_rx),
      port_rxb(1 << _port_rx),
      port_txb(1 << _port_tx)
      {}
    #endif

    #if defined(ARDUINO_ESP8266_NODEMCU) || defined(ARDUINO_ESP32_DEV)
      EzAvcLan(
        const uint8_t &_port_tx, // Port number on esp8266
        const uint8_t &_port_rx // Port number on esp8266
      ) :
      port_rx(_port_rx),
      port_tx(_port_tx)
      {}
    #endif

    void start();
    void sendFrame(const AvcLanFrame &frame);
    ReadError readFrame(AvcLanFrame *frame);

  private:
    uint8_t lastSendBit = 0;
    uint8_t Gi = 0; // Global i iterator counter

    #ifdef ARDUINO_AVR_UNO
      // 16MHz, with prescaler 1, (tick every 0.0625 us), values are adjusted to inaccuracy
      static constexpr uint16_t _3us_timer_value        =   50;          
      static constexpr uint16_t _5us_timer_value        =   70;          
      static constexpr uint16_t _7us_timer_value        =   84;          
      static constexpr uint16_t _15us_timer_value       =  240;
      static constexpr uint16_t _17us_timer_value       =  270;
      static constexpr uint16_t _20us_timer_value       =  293;
      static constexpr uint16_t _almost20us_timer_value =  285;
      static constexpr uint16_t _33us_timer_value       =  500;
      static constexpr uint16_t _200us_timer_value      = 3200;
    #endif

    #if defined(ARDUINO_ESP8266_NODEMCU) || defined(ARDUINO_ESP32_DEV)
      // 80MHz, with prescaler 1, (tick every 0.0125 us)
      static constexpr uint16_t _3us_timer_value        = microsecondsToClockCycles(3);          
      static constexpr uint16_t _5us_timer_value        = microsecondsToClockCycles(5);          
      static constexpr uint16_t _7us_timer_value        = microsecondsToClockCycles(7);          
      static constexpr uint16_t _15us_timer_value       = microsecondsToClockCycles(15);
      static constexpr uint16_t _17us_timer_value       = microsecondsToClockCycles(17);
      static constexpr uint16_t _20us_timer_value       = microsecondsToClockCycles(20);
      static constexpr uint16_t _almost20us_timer_value = microsecondsToClockCycles(20);
      static constexpr uint16_t _33us_timer_value       = microsecondsToClockCycles(33);
      static constexpr uint16_t _200us_timer_value      = microsecondsToClockCycles(20);
    #endif

    static constexpr uint8_t master_address_length = 12;
    static constexpr uint8_t slave_address_length  = 12;
    static constexpr uint8_t control_length        =  4;
    static constexpr uint8_t data_length_length    =  8;
    static constexpr uint8_t data_length           =  8;

    inline volatile uint8_t is_high();
    inline volatile uint8_t is_low();

    const uint8_t port_tx;
    const uint8_t port_rx;

    #if defined(ARDUINO_ESP8266_NODEMCU) || defined(ARDUINO_ESP32_DEV)
      uint32_t lastTimerValue; 
    #endif

    #ifdef ARDUINO_AVR_UNO
      const uint8_t port_tx_arduino;
      const uint8_t port_rx_arduino;
      const uint8_t port_rxb;
      const uint8_t port_txb;
      volatile uint8_t* ports_tx;
      volatile uint8_t* ports_rx;
    #endif

    // Write Part
    inline void reset_timer();
    inline volatile uint16_t read_timer();
    inline void set_high();
    inline void set_low();
    void prepareTimer();
    void preparePins();
    inline void ready_to_send_next_bit();
    inline void last_bit_ended();
    void finish();
    void sendBit(const uint8_t &bit);
    void sendHeaderBit(const uint8_t &bit);
    void sendBits(const uint32_t &bits, const uint8_t &length);
    // Returns 1 if partity, 0 if not parity
    uint8_t calcParity(const uint32_t &n);
    void sendAck();

    // Read part
    EzAvcLan::ReadError readBit(uint8_t *bit);
    // EzAvcLan::ReadError readBits(uint16_t *bits, const uint8_t &length);
    template<class T> EzAvcLan::ReadError EzAvcLan::readBits(T *bits, const uint8_t &length);
    EzAvcLan::ReadError readBitLongWait(uint8_t *bit);
    EzAvcLan::ReadError readBitValue(uint8_t *bit);
    EzAvcLan::ReadError acknowledge();
};
